# picardtools
Python3 interface with [Picard Tools](https://broadinstitute.github.io/picard/)
from Broad institute.

## Installation

First, install the python package
```sh
pip install picardtools
```
or
```sh
pip install --user picardtools
```

Then download the Picard Tools `.jar` file
```sh
picardtools-download
```
